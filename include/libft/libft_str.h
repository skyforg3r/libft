/*
 *  include/libft/libft_str.h
 *
 *  (Header file) Functions for working with strings.
 * 
 *  Copyright (C) 2023 skyforg3r
 * 
 * This file is part of the libft implementation by @skyforg3r.
 * All source code in this repository may be redistributed
 * and/or modified under the terms of the MIT License.
 * 
 * See LICENSE.txt for the detailed license terms.
 */

#ifndef ___LIBFT__LIBFT_STR_H___
#define ___LIBFT__LIBFT_STR_H___

#include <stddef.h>

int ft_isalpha(int c);

int ft_isdigit(int c);

int ft_isalnum(int c);

int ft_isascii(int c);

int ft_isprint(int c);

int ft_strlen(const char* s);

size_t ft_strlcpy(char* dst, const char* src, size_t size);

size_t ft_strlcat(char* dst, const char* src, size_t size);

int toupper(int c);

int tolower(int c);

char* strchr(const char* s, int c);

char* strrchr(const char* s, int c);

char* strstr(const char* s1, const char* s2);

int atoi(const char* nptr);

char* strdup(const char* s);

char* ft_substr(const char* s, unsigned int start, size_t len);

char* ft_strjoin(const char* s1, const char* s2);

char* ft_strtrim(const char* s1, const char* set);

char** ft_split(const char* s, char c);

char* ft_itoa(int n);

char* ft_strmapi(const char* s, char(*f)(unsigned int, char));

void ft_striteri(char* s, void(*f)(unsigned int, char*));

#endif
/* ___LIBFT__LIBFT_STR_H___ */