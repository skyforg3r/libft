/*
 *  include/libft/libft_mem.h
 *
 *  (Header file) Functions for working with memory buffers.
 * 
 *  Copyright (C) 2023 skyforg3r
 * 
 * This file is part of the libft implementation by @skyforg3r.
 * All source code in this repository may be redistributed
 * and/or modified under the terms of the MIT License.
 * 
 * See LICENSE.txt for the detailed license terms.
 */

#ifndef ___LIBFT__LIBFT_MEM_H___
#define ___LIBFT__LIBFT_MEM_H___

#include <stddef.h>

/**
 * Copies `n` bytes from memory area `src` to memory area `dst`. The memory areas
 * must not overlap.
 * @param dst The destination buffer
 * @param src The source buffer
 * @param n The amount of bytes to copy
 * @return A pointer to the destination buffer
 */
void* ft_memcpy(void* dst, const void* src, size_t n);

/**
 * Fills the first `n` bytes of the memory area pointed to by `s` with the constant
 * byte `c`.
 * @param s The destination buffer
 * @param c The byte that will be copied to the destination buffer
 * @param n The amount of bytes from the destination buffer that will be filled
 * @return A pointer to the destination buffer
 */
void* ft_memset(void* s, int c, size_t n);

/**
 * Erases the data in the `n` bytes of memory starting at the location pointed to by
 * `s`, by writing zeros to that area.
 * @param s The buffer to erase
 * @param n The amount of bytes to erase
 */
void ft_bzero(void* s, size_t n);

/**
 * Copies `n` bytes from memory area `src` to memory area `dst`. The memory areas
 * may overlap: copying takes place as though the bytes in `src` are first copied
 * to a temporary array that does not overlap `src` or `dst`, and the bytes are then
 * copied from the temporary array to `dst`.
 * @param dst The destination buffer
 * @param src The source buffer
 * @param n The amount of bytes to copy
 * @return A pointer to the destination buffer
 */
void* ft_memmove(void* dst, const void* src, size_t n);

/**
 * Scans the initial `n` bytes of the memory area pointed to by `s` for the first
 * instance of `c`. Both `c` and the bytes of the memory area pointed to by `s`are
 * interpreted as `unsigned char`.
 * @param s The buffer to scan
 * @param c The byte to search for
 * @param n The amount of bytes to scan, from the beginning of the buffer
 */
void* ft_memchr(const void* s, int c, size_t n);

/**
 * Returns an integer less than, equal to, or greater than zero if the first `n`
 * bytes of `s1` is found, respectively, to be less than, to match, or be greater
 * than the first `n` bytes of `s2`.
 * @param s1 The first buffer to compare
 * @param s2 The second buffer to compare
 * @param n The amount of bytes to compare
 * @return An integer with a sign that represents the result of the comparison
 */
int ft_memcmp(const void* s1, const void* s2, size_t n);

/**
 * Allocates memory for an array of `nmemb` elements of `size` bytes each, and
 * returns a pointer to the allocated memory. The memory is set to zero. If `nmemb`
 * or size is 0, then `calloc()` returns NULL. If the multiplication of `nmemb` and
 * `size` would result in integer overflow, then `calloc()` returns NULL.
 * @param nmemb The amount of elements to allocate memory for
 * @param size The size of each element
 */
void* ft_calloc(size_t nmemb, size_t size);

#endif
/* ___LIBFT__LIBFT_MEM_H___ */