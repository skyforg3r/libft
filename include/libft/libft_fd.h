/*
 *  include/libft/libft_fd.h
 *
 *  (Header file) Functions for working with POSIX file descriptors.
 * 
 *  Copyright (C) 2023 skyforg3r
 * 
 * This file is part of the libft implementation by @skyforg3r.
 * All source code in this repository may be redistributed
 * and/or modified under the terms of the MIT License.
 * 
 * See LICENSE.txt for the detailed license terms.
 */

#ifndef ___LIBFT__LIBFT_FD_H___
#define ___LIBFT__LIBFT_FD_H___

void ft_putstr_fd(char* s, int fd);

void ft_putendl_fd(char* s, int fd);

void ft_putnbr_fd(int n, int fd);

#endif
/* ___LIBFT__LIBFT_FD_H___ */