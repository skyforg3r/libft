/*
 *  include/libft/libft_bonus.h
 *
 *  (Header file) Interface for working with linked lists.
 * 
 *  Copyright (C) 2023 skyforg3r
 * 
 * This file is part of the libft implementation by @skyforg3r.
 * All source code in this repository may be redistributed
 * and/or modified under the terms of the MIT License.
 * 
 * See LICENSE.txt for the detailed license terms.
 */

#ifndef ___LIBFT__LIBFT_BONUS_H___
#define ___LIBFT__LIBFT_BONUS_H___

typedef struct s_list {
    void* content;
    struct s_list* next;
} t_list;

t_list* ft_lstnew(void* content);

void ft_lstadd_front(t_list** lst, t_list* new_lst);

void ft_lstsize(t_list* lst);

t_list* ft_lstlast(t_list* lst);

void ft_lstadd_back(t_list** lst, t_list* new_lst);

void ft_lstdelone(t_list* lst, void(*del)(void*));

void ft_lstclear(t_list** lst, void(*del)(void*));

void ft_lstiter(t_list* lst, void(*f)(void*));

void ft_lstmap(t_list* lst, void*(*f)(void*), void(*del)(void*));

#endif
/* ___LIBFT__LIBFT_BONUS_H___ */