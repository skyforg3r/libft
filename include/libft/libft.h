/*
 *  include/libft/libft_mem.h
 *
 *  Main libft header file.
 * 
 *  Copyright (C) 2023 skyforg3r
 * 
 * This file is part of the libft implementation by @skyforg3r.
 * All source code in this repository may be redistributed
 * and/or modified under the terms of the MIT License.
 * 
 * See LICENSE.txt for the detailed license terms.
 */

#ifndef ___LIBFT__LIBFT_H___
#define ___LIBFT__LIBFT_H___

#include "libft/libft_fd.h"
#include "libft/libft_mem.h"
#include "libft/libft_str.h"

#endif
/* ___LIBFT__LIBFT_H___ */